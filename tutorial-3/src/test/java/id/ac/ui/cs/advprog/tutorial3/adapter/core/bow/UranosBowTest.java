package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class UranosBowTest {
    private Class<?> uranosBowClass;

    @BeforeEach
    public void setUp() throws Exception {
        uranosBowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow");
    }

    @Test
    public void testUranosBowIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(uranosBowClass.getModifiers()));
    }

    @Test
    public void testUranosBowIsABow() {
        Collection<Type> interfaces = Arrays.asList(uranosBowClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow")));
    }

    @Test
    public void testUranosBowOverrideShootArrowMethod() throws Exception {
        Class<?>[] shootArrowArgs = new Class[1];
        shootArrowArgs[0] = boolean.class;
        Method shootArrow = uranosBowClass.getDeclaredMethod("shootArrow", shootArrowArgs);

        assertEquals("java.lang.String",
                shootArrow.getGenericReturnType().getTypeName());
        assertEquals(1,
                shootArrow.getParameterCount());
        assertTrue(Modifier.isPublic(shootArrow.getModifiers()));
    }

    @Test
    public void testUranosBowOverrideGetNameMethod() throws Exception {
        Method getName = uranosBowClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testUranosBowOverrideGetHolderMethod() throws Exception {
        Method getHolderName = uranosBowClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testUranosBowNormalAttack() throws Exception {
       UranosBow uranosBow = new UranosBow("Dummy Mc Dumb Dumb");
       String normalAttack = uranosBow.shootArrow(false);
       assertEquals("headshot!", normalAttack);
    }

    @Test
    public void testUranosBowChargedAttack() throws Exception {
        UranosBow uranosBow = new UranosBow("Dummy Mc Dumb Dumb");
        String chargedAttack = uranosBow.shootArrow(true);
        assertEquals("Gaining charge... gaining speed... headshot!", chargedAttack);
    }

    @Test
    public void testUranosGetHolderName() throws Exception {
        UranosBow uranosBow = new UranosBow("Dummy Mc Dumb Dumb");
        String holderName = uranosBow.getHolderName();
        assertEquals("Dummy Mc Dumb Dumb", holderName);
    }

    @Test
    public void testUranosBowGetName() throws Exception {
        UranosBow uranosBow = new UranosBow("Dummy Mc Dumb Dumb");
        String name = uranosBow.getName();
        assertEquals("Uranos Bow", name);
    }
}
