package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class BowAdapterTest {
    private Class<?> bowAdapterClass;
    private Class<?> bowClass;

    @BeforeEach
    public void setUp() throws Exception {
        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
    }

    @Test
    public void testBowAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(bowAdapterClass.getModifiers()));
    }

    @Test
    public void testBowAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(bowAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testBowAdapterConstructorReceivesBowAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = bowClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                bowAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testBowAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = bowAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = bowAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetNameMethod() throws Exception {
        Method getName = bowAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = bowAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testBowAdapterIonicBowNormalAttack() {
        BowAdapter bowAdapter = new BowAdapter(new IonicBow("Dummy Mc Dumb Dumb"));
        String normalAttack = bowAdapter.normalAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Ionic Bow (normal attack): " +
                "Separated one atom from the enemy", normalAttack);
    }

    @Test
    public void testBowAdapterIonicBowChargedAttack() {
        BowAdapter bowAdapter = new BowAdapter(new IonicBow("Dummy Mc Dumb Dumb"));
        String chargedAttack = bowAdapter.chargedAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Ionic Bow (charged attack): " +
                "Entered aim shot mode", chargedAttack);
    }

    @Test
    public void testBowAdapterIonicBowChargedAttackThenNormalAttack() {
        BowAdapter bowAdapter = new BowAdapter(new IonicBow("Dummy Mc Dumb Dumb"));
        bowAdapter.chargedAttack();
        String normalAttack = bowAdapter.normalAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Ionic Bow (normal attack): " +
                "Arrow reacted with the enemy's protons", normalAttack);
    }

    @Test
    public void testBowAdapterIonicBowChargedAttackThenChargedAttack() {
        BowAdapter bowAdapter = new BowAdapter(new IonicBow("Dummy Mc Dumb Dumb"));
        bowAdapter.chargedAttack();
        String chargedAttack = bowAdapter.chargedAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Ionic Bow (charged attack): " +
                "Leaving aim shot mode", chargedAttack);
    }

    @Test
    public void testBowAdapterIonicBowGetName() {
        BowAdapter bowAdapter = new BowAdapter(new IonicBow("Dummy Mc Dumb Dumb"));
        String normalAttack = bowAdapter.getName();
        assertEquals("Ionic Bow", normalAttack);
    }

    @Test
    public void testBowAdapterIonicBowGetHolderName() {
        BowAdapter bowAdapter = new BowAdapter(new IonicBow("Dummy Mc Dumb Dumb"));
        String normalAttack = bowAdapter.getHolderName();
        assertEquals("Dummy Mc Dumb Dumb", normalAttack);
    }

    // Uranos
    @Test
    public void testBowAdapterUranosBowNormalAttack() {
        BowAdapter bowAdapter = new BowAdapter(new UranosBow("Dummy Mc Dumb Dumb"));
        String normalAttack = bowAdapter.normalAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Uranos Bow (normal attack): " +
                "headshot!", normalAttack);
    }

    @Test
    public void testBowAdapterUranosBowChargedAttack() {
        BowAdapter bowAdapter = new BowAdapter(new UranosBow("Dummy Mc Dumb Dumb"));
        String chargedAttack = bowAdapter.chargedAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Uranos Bow (charged attack): " +
                "Entered aim shot mode", chargedAttack);
    }

    @Test
    public void testBowAdapterUranosBowChargedAttackThenNormalAttack() {
        BowAdapter bowAdapter = new BowAdapter(new UranosBow("Dummy Mc Dumb Dumb"));
        bowAdapter.chargedAttack();
        String normalAttack = bowAdapter.normalAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Uranos Bow (normal attack): " +
                "Gaining charge... gaining speed... headshot!", normalAttack);
    }

    @Test
    public void testBowAdapterUranosBowChargedAttackThenChargedAttack() {
        BowAdapter bowAdapter = new BowAdapter(new UranosBow("Dummy Mc Dumb Dumb"));
        bowAdapter.chargedAttack();
        String chargedAttack = bowAdapter.chargedAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Uranos Bow (charged attack): " +
                "Leaving aim shot mode", chargedAttack);
    }

    @Test
    public void testBowAdapterUranosBowGetName() {
        BowAdapter bowAdapter = new BowAdapter(new UranosBow("Dummy Mc Dumb Dumb"));
        String normalAttack = bowAdapter.getName();
        assertEquals("Uranos Bow", normalAttack);
    }

    @Test
    public void testBowAdapterUranosBowGetHolderName() {
        BowAdapter bowAdapter = new BowAdapter(new UranosBow("Dummy Mc Dumb Dumb"));
        String normalAttack = bowAdapter.getHolderName();
        assertEquals("Dummy Mc Dumb Dumb", normalAttack);
    }
}
