package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class FullMoonPikeTest {
    private Class<?> fullMoonPikeClass;

    @BeforeEach
    public void setUp() throws Exception {
        fullMoonPikeClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.FullMoonPike");
    }

    @Test
    public void testFullMoonPikeIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(fullMoonPikeClass.getModifiers()));
    }

    @Test
    public void testFullMoonPikeIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(fullMoonPikeClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testFullMoonPikeOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = fullMoonPikeClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testFullMoonPikeOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = fullMoonPikeClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testFullMoonPikeOverrideGetNameMethod() throws Exception {
        Method getName = fullMoonPikeClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testFullMoonPikeOverrideGetHolderMethod() throws Exception {
        Method getHolderName = fullMoonPikeClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testFullMoonPikeNormalAttack() throws Exception {
        Weapon weapon = new FullMoonPike("Dummy Mc Dumb Dumb");
        String normalAttack = weapon.normalAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Full Moon Pike (normal attack): " +
                "ara ara", normalAttack);
    }

    @Test
    public void testFullMoonPikeChargedAttack() throws Exception {
        Weapon weapon = new FullMoonPike("Dummy Mc Dumb Dumb");
        String chargedAttack = weapon.chargedAttack();
        assertEquals("Dummy Mc Dumb Dumb attacked with Full Moon Pike (charged attack): " +
                "ara kono hentai yaro", chargedAttack);
    }

    @Test
    public void testFullMoonPikeGetHolderName() throws Exception {
        Weapon weapon = new FullMoonPike("Dummy Mc Dumb Dumb");
        String holderName = weapon.getHolderName();
        assertEquals("Dummy Mc Dumb Dumb", holderName);
    }
    @Test
    public void testFullMoonPikeGetName() throws Exception {
        Weapon weapon = new FullMoonPike("Dummy Mc Dumb Dumb");
        String name = weapon.getName();
        assertEquals("Full Moon Pike", name);
    }
}
