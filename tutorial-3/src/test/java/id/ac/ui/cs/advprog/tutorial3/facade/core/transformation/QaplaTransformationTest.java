package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QaplaTransformationTest {
    private Class<?> qaplaClass;

    @BeforeEach
    public void setup() throws Exception {
        qaplaClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.QaplaTransformation");
    }

    @Test
    public void testQaplaHasEncodeMethod() throws Exception {
        Method translate = qaplaClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testQaplaEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "CKPSbKuKXNu3ugOXdudYuKuLVKMUcWSdRudYuPYbQOuYebucgYbN";

        Spell result = new QaplaTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testQaplaEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "GOTWfOyObRy7ykSbhyhcyOyPZOQYgaWhVyhcyTcfUSycifygkcfR";

        Spell result = new QaplaTransformation(12).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testQaplaHasDecodeMethod() throws Exception {
        Method translate = qaplaClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testQaplaDecodesCorrectly() throws Exception {
        String text = "CKPSbKuKXNu3ugOXdudYuKuLVKMUcWSdRudYuPYbQOuYebucgYbN";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new QaplaTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testQaplaDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "GOTWfOyObRy7ykSbhyhcyOyPZOQYgaWhVyhcyTcfUSycifygkcfR";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new QaplaTransformation(12).decode(spell);
        assertEquals(expected, result.getText());
    }
}
