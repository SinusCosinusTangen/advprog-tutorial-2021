package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }
    @Override
    public String normalAttack() {
        // TODO: complete me
        return getHolderName() + " attacked with " + getName() + " (normal attack): Watashi wa anata o daikirai";
    }

    @Override
    public String chargedAttack() {
        // TODO: complete me
        return getHolderName() + " attacked with " + getName() + " (charged attack): Watashi wa anata o daisuki";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
