package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.*;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.*;

public class TransformationHelper {
    private static AbyssalTransformation abyssalTransformation = new AbyssalTransformation();
    private static CelestialTransformation celestialTransformation = new CelestialTransformation();
    private static QaplaTransformation qaplaTransformation = new QaplaTransformation();

//    public TransformationHelper() {
//        abyssalTransformation = new AbyssalTransformation();
//        celestialTransformation =
//    }

    public static String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        spell = qaplaTransformation.encode(spell);
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);

        return spell.getText();
    }

    public static String decode(String text) {
        Spell spell = new Spell(text, RunicCodex.getInstance());
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        spell = qaplaTransformation.decode(spell);
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());

        return spell.getText();
    }
}
