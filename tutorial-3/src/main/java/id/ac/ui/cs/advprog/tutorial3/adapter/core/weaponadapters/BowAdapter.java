package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimShot = false;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return getHolderName() + " attacked with " + getName() + " (normal attack): " + bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        String enter = "";
        if (this.isAimShot == true) {
            isAimShot = false;
            enter = "Leaving aim shot mode";
        } else {
            isAimShot = true;
            enter = "Entered aim shot mode";
        }
        return getHolderName() + " attacked with " + getName() + " (charged attack): " + enter;
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
