package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class QaplaTransformation {
    private int key;

    public QaplaTransformation(int key){
        this.key = key;
    }

    public QaplaTransformation(){
        this(16);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? -1 : 1;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            int newIdx = codex.getIndex(text.charAt(i)) + this.key * selector;
//            newIdx = newIdx < 0 ? newIdx + codex.getCharSize() : newIdx - codex.getCharSize();
            if (newIdx < 0) {
                newIdx += codex.getCharSize();
            } else if(newIdx >= codex.getCharSize()) {
                newIdx -= codex.getCharSize();
            }
            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);
    }
}
