package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean charged = true;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        charged = true;
        return getHolderName() + " attacked with " + getName() + " (normal attack): " + spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (this.charged == true) {
            charged = false;
            return getHolderName() + " attacked with " + getName() + " (charged attack): " + spellbook.largeSpell();
        }
        return getHolderName() + " attacked with " + getName() + " (charged attack): not enough mana!";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
