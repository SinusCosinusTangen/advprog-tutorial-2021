package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.AdapterInitializer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        for (Spellbook sb:spellbookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(sb));
        }

        for (Bow b:bowRepository.findAll()) {
            weaponRepository.save(new BowAdapter(b));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        if (weaponRepository.findByAlias(weaponName) != null) {
            Weapon weapon = weaponRepository.findByAlias(weaponName);
            if (attackType == 1) {
                logRepository.addLog(weapon.normalAttack());
            } else {
                logRepository.addLog(weapon.chargedAttack());
            }
            weaponRepository.save(weapon);
        }
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
