package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    private String name;
    private AttackBehavior attackBehavior;
    private DefenseBehavior defenseBehavior;
    //ToDo: Complete me
    public AgileAdventurer() {
        this.name = "Agile";
        this.attackBehavior = new AttackWithGun();
        this.defenseBehavior = new DefendWithBarrier();
        this.setAttackBehavior(this.attackBehavior);
        this.setDefenseBehavior(this.defenseBehavior);
    }

    @Override
    public String getAlias() {
        return name;
    }
}
