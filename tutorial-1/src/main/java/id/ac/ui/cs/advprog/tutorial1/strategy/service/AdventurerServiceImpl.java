package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        //ToDo: Complete me
        Adventurer adventurer = findByAlias(alias);
        AttackBehavior attackBehavior = adventurer.getAttackBehavior();
        DefenseBehavior defenseBehavior = adventurer.getDefenseBehavior();
        if (attackType.equals("Senjata Api")) {
            attackBehavior = strategyRepository.getAttackBehaviorByType(attackType);
        } else if (attackType.equals("Pedang")) {
            attackBehavior = strategyRepository.getAttackBehaviorByType(attackType);
        } else {
            attackBehavior = strategyRepository.getAttackBehaviorByType(attackType);
        }

        if (defenseType.equals("Shield")) {
            defenseBehavior = strategyRepository.getDefenseBehaviorByType(defenseType);
        } else if (defenseType.equals("Barrier")) {
            defenseBehavior = strategyRepository.getDefenseBehaviorByType(defenseType);
        } else {
            defenseBehavior = strategyRepository.getDefenseBehaviorByType(defenseType);
        }
        adventurer.setAttackBehavior(attackBehavior);
        adventurer.setDefenseBehavior(defenseBehavior);
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
