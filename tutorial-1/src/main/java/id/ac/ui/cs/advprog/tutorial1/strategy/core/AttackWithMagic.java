package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    private String type;
    public AttackWithMagic() {
        this.type = "Sihir";
    }

    @Override
    public String attack() {
        return "Swhozzzzz";
    }

    @Override
    public String getType() {
        return type;
    }
}
