package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    private String type;
    //ToDo: Complete me
    public AttackWithGun() {
        this.type = "Senjata Api";
    }

    @Override
    public String attack() {
        return "Dor";
    }

    @Override
    public String getType() {
        return type;
    }
}
