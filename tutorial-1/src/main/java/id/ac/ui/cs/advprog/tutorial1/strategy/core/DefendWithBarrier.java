package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    private String name;
    public DefendWithBarrier() {
        this.name = "Barrier";
    }

    @Override
    public String defend() {
        return "Donggg";
    }

    @Override
    public String getType() {
        return name;
    }
}
