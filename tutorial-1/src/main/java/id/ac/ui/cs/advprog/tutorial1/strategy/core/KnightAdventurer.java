package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    //ToDo: Complete me
    private String name;
    private AttackBehavior attackBehavior;
    private DefenseBehavior defenseBehavior;
    public KnightAdventurer() {
        this.name = "Knight";
        this.attackBehavior = new AttackWithSword();
        this.defenseBehavior = new DefendWithArmor();
        this.setAttackBehavior(this.attackBehavior);
        this.setDefenseBehavior(this.defenseBehavior);
    }

    @Override
    public String getAlias() {
        return name;
    }
}
