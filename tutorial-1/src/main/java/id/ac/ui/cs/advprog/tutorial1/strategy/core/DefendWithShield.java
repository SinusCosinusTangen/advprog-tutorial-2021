package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me
    private String name;
    public DefendWithShield() {
        this.name = "Shield";
    }

    @Override
    public String defend() {
        return "Srakkkk";
    }

    @Override
    public String getType() {
        return name;
    }
}
