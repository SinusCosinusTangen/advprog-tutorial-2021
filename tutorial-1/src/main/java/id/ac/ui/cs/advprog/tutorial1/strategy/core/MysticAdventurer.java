package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    //ToDo: Complete me
    private String name;
    private AttackBehavior attackBehavior;
    private DefenseBehavior defenseBehavior;
    public MysticAdventurer() {
        this.name = "Mystic";
        this.attackBehavior = new AttackWithMagic();
        this.defenseBehavior = new DefendWithShield();
        this.setAttackBehavior(this.attackBehavior);
        this.setDefenseBehavior(this.defenseBehavior);
    }

    @Override
    public String getAlias() {
        return name;
    }
}
