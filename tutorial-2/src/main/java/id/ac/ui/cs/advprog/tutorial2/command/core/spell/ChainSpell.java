package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spell;
    public ChainSpell(ArrayList<Spell> spell) {
        this.spell = spell;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for (Spell s:spell) {
            s.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spell.size() - 1; i >= 0; i--) {
            spell.get(i).undo();
        }
    }
}
