package id.ac.ui.cs.advprog.tutorial2.template.core;

import java.util.List;

public class Saber extends SpiritInQuest {
//    @Override
//    public List attackPattern() {
//        List list = super.attackPattern();
//        list.add(buff());
//        list.add(attackWithBuster());
//        list.add(attackWithQuick());
//        list.add(attackWithSpecialSkill());
//        return list;
//    }

    @Override
    protected String buff() {
        return "Buff Quick Attack with 100% Damage";
    }

    @Override
    protected String attackWithBuster() {
        return "Attack with Buster";
    }

    @Override
    protected String attackWithQuick() {
        return "Attack with Quick + 100% Damage";
    }

    @Override
    protected String attackWithArts() {
        return "Attack with Arts";
    }

    @Override
    protected String attackWithSpecialSkill() {
        return "Sreeettt Sreeettt";
    }

}